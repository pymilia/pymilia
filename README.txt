This is PyMilia, a set of Python bindings for milia

Milia is library that provides distances and ages in cosmology

Pymilia requires a functional milia installation, and 
boost (http://www.boost.org/), a C++ library that provides the wrapping library.

This package is distributed under GPL , either version 3 of the License, or
(at your option) any later version. See the file COPYING for details.

Maintainer: sergiopr@astrax.fis.ucm.es
Webpage: https://halmax.fis.ucm.es/projects/milia/wiki

Installation
------------

1. Download pymilia-<version>.tar.gz.

2. Extract the archive to a temporary directory.

3. Install by changing to the directory and typing "python setup.py install"

----------------------

Nicolas:
--------

To install not system wide:

CFLAGS=-I/home/gruel/usr/include LDFLAGS=-L/home/gruel/usr/lib python setup.py install --prefix=/home/gruel/usr
