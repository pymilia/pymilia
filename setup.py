#!/usr/bin/env python
# add: include_dirs and library_dirs for local installation.
# change: boost_python by boost_python-mt for ubuntu

from distutils.core import setup, Extension

ext1 = Extension('milia.metrics', ['src/metrics_wrap.cc'], 
  include_dirs =  ['/home/gruel/usr/include'],
  library_dirs = ['/usr/lib','/home/gruel/usr/lib'],
  libraries = ['boost_python-mt', 'milia', 'gsl', 'gslcblas'])
ext2 = Extension('milia.lumfuncs', ['src/lumfuncs_wrap.cc'], 
  include_dirs =  ['/home/gruel/usr/include'],
  library_dirs = ['/usr/lib','/home/gruel/usr/lib'],
  libraries = ['boost_python-mt', 'milia', 'gsl', 'gslcblas'])

setup(name='pymilia',
      version='0.2.2',
      author='Sergio Pascual',
      author_email='sergiopr@astrax.fis.ucm.es',
      url='https:// halmax.fis.ucm.es/projects/milia/wiki',
      license='GPLv3',
      description='Cosmological distances and ages',
      package_dir={'milia': 'lib/milia'},
      packages=['milia'],
      ext_modules=[ext1, ext2],
      )
